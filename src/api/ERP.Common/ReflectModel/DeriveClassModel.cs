﻿using System.Collections.Generic;

namespace ERP.Common.ReflectModel
{
    /// <summary>
    /// 派生类实体
    /// </summary>
    public class DeriveClassModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string DeriveClassName { get; set; }

        /// <summary>
        /// 方法集合
        /// </summary>
        public List<MethodModel> Methods { get; set;} = new List<MethodModel>();

    }
}