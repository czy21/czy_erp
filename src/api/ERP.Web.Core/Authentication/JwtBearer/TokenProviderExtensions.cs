﻿using Microsoft.AspNetCore.Builder;

namespace ERP.Web.Core.Authentication.JwtBearer
{
    public static class TokenProviderExtensions
    {
        public static IApplicationBuilder UseJwtAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TokenProviderMiddleware>();
        }
    }
}