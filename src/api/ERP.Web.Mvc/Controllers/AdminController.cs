﻿using ERP.Common.Utils;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    [EnableCors("MyDomain")]
    [Route("api/[controller]")]
    public class AdminController : Controller
    {
        [HttpGet("LoadController")]
        public IActionResult LoadController()
        {
            var controllerList = ReflectHelper.GetControllerInfo(typeof(OperationControllerBase));
            return Json(new { Success = true, Result = controllerList });
        }
    }
}