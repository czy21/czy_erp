﻿using System;
using ERP.BLL.BaseApp;
using ERP.BLL.DepotApp.Dtos;
using ERP.Common.Model;

// ReSharper disable All

namespace ERP.BLL.DepotApp
{
    public interface IDepotService : IServiceBase
    {
        /// <summary>
        /// 获取分页仓库列表
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页数</param>
        /// <returns></returns>
        PagedResultModel<DepotDto> GetDepotPageList(int pageIndex, int pageSize);

        /// <summary>
        /// 更新仓库
        /// </summary>
        /// <param name="dto">仓库实体</param>
        /// <returns></returns>
        int Insert(DepotDto dto);

        /// <summary>
        /// 更新仓库
        /// </summary>
        /// <param name="dto">仓库实体</param>
        /// <returns></returns>
        bool Update(DepotDto dto);

        /// <summary>
        /// 根据Id删除仓库
        /// </summary>
        /// <param name="id">仓库Id</param>
        void Delete(Guid id);
    }
}