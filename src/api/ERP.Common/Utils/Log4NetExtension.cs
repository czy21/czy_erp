﻿using System;
using System.Reflection;
using log4net;

namespace ERP.Common.Utils
{
    public class Log4NetExtension
    {
        /// <summary>
        /// Info级别日志前缀
        /// </summary>
        public static string LOG_INFO_PREFIX = "============";

        /// <summary>
        /// Error级别日志前缀
        /// </summary>
        public static string LOG_ERROR_PREFIX = ">>>>>>>>>>>";

        /// <summary>
        /// Debug级别日志前缀
        /// </summary>
        public static string LOG_DEBUG_PREFIX = "-----------";

        /// <summary>
        /// Fatal级别日志前缀
        /// </summary>
        public static string LOG_FATAL_PREFIX = "$$$$$$$$$$$";

        /// <summary>
        /// warn级别日志前缀
        /// </summary>
        public static string LOG_WARN_PREFIX = "############";

        /// <summary>
        /// 根据异常信息获取调用类的信息
        /// </summary>
        private static readonly Exception _ex = new Exception();


        /// <summary>
        /// 获取log4net实例
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("NETCoreRepository", MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 打印Deug日期信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        public static void DebugFileSync(object msg)
        {
            Log.Debug(msg);
        }

        /// <summary>
        /// 打印Debug异常信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="e">异常堆栈</param>
        public static void DebugFileSync(object msg, Exception e)
        {
            Log.Debug(msg, e);
        }
        /// <summary>
        /// 打印Info日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        public static void InfoFileSync(object msg)
        {
            Log.Info(msg);
        }

        /// <summary>
        /// 打印Info日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="e">异常堆栈</param>
        public static void InfoFileSync(object msg, Exception e)
        {
            Log.Info(msg, e);
        }

        /// <summary>
        /// 打印Warn日期信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        public static void WarnFileSync(object msg)
        {
            Log.Warn(msg);
        }
        /// <summary>
        /// 打印Warn日期信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="e">异常堆栈</param>
        public static void WarnFileSync(object msg, Exception e)
        {
            Log.Warn(msg, e);
        }

        /// <summary>
        /// 打印Error日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        public static void ErrorFileSync(object msg)
        {
            Log.Error(msg);
        }
        /// <summary>
        /// 打印Error日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="e">异常堆栈</param>
        public static void ErrorFileSync(object msg, Exception e)
        {
            Log.Error(msg, e);
        }

        /// <summary>
        /// 打印Fatal日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        public static void FatalFileSync(object msg)
        {
            Log.Fatal(msg);
        }

        /// <summary>
        /// 打印Fatal日志信息
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="e">异常堆栈</param>
        public static void FatalFileSync(object msg, Exception e)
        {
            Log.Fatal(msg, e);
        }
    }
}
