﻿using System;
using System.Collections.Generic;
using ERP.BLL.BaseApp;
using ERP.BLL.UserApp.Dtos;
using ERP.Common.Model;

namespace ERP.BLL.UserApp
{
    public interface IUserService : IServiceBase
    {
        /// <summary>
        /// 判断用户名是否符合登录条件
        /// </summary>
        /// <param name="loginName">账号</param>
        /// <param name="password">密码</param>
        /// <returns>1、用户名不存在  2、密码不正确  3、黑名单用户  4、符合条件  5、访问后台异常</returns>
        int ValidateUser(string loginName, string password);

        /// <summary>
        /// 根据登陆名称获取用户信息
        /// </summary>
        /// <param name="loginName">登录名称</param>
        /// <returns></returns>
        UserDto GetUser(string loginName);

        /// <summary>
        /// 根据用户id获取实体
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        UserDto GetUser(Guid id);

        /// <summary>
        /// 获取分页用户列表
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">单页大小</param>
        /// <returns></returns>
        PagedResultModel<UserDto> GetUserPageList(int pageIndex, int pageSize);

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="dto">用户实体</param>
        /// <returns>默认可以添加=0,重号=1,重名=2</returns>
        int Insert(UserDto dto);

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="dto">实体</param>
        /// <returns>bool</returns>
        bool Update(UserDto dto);

        /// <summary>
        /// 根据Id集合批量删除
        /// </summary>
        /// <param name="ids">Id集合</param>
        void DeleteBatch(List<Guid> ids);

        /// <summary>
        /// 根据Id删除用户
        /// </summary>
        /// <param name="userId">用户Id</param>
        void Delete(Guid userId);

        /// <summary>
        /// 检查用户属性值是否存在
        /// </summary>
        /// <param name="attr">用户属性</param>
        /// <param name="attrValue">属性值</param>
        /// <returns>true==存在重名 false==不存在</returns>
        bool CheckIsNameExist(string attr, string attrValue);

        /// <summary>
        /// 获取用户角色集合
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        List<Guid> GetRolesByUser(Guid userId);


        /// <summary>
        /// 更新用户角色关联关系
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="userRoles">用户角色集合</param>
        /// <returns></returns>
        bool UpdateUserRole(Guid userId, List<UserRoleDto> userRoles);

    }
}