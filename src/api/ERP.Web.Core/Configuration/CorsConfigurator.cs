﻿using Microsoft.Extensions.DependencyInjection;

namespace ERP.Web.Core.Configuration
{
    public static class CorsConfigurator
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            services.AddCors(options =>
                options.AddPolicy("MyDomain", it =>
                     it.WithOrigins("http://47.106.191.56:8089").AllowAnyHeader().AllowAnyMethod().AllowCredentials()));
            return services;
        }
    }
}
