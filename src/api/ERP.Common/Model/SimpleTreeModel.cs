﻿using System;

namespace ERP.Common.Model
{
    public class SimpleTreeModel
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
    }
}
