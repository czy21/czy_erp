﻿
using System;

namespace ERP.BLL.BaseApp.Dtos
{
    public class BaseDto<TPrimaryKey>
    {
        /// <summary>
        /// 主键
        /// </summary>
        public TPrimaryKey Id { get; set; }
        public DateTime AddedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        public bool Enabled { get; set; }
    }

    /// <summary>
    /// 定义默认主键类型为long的实体基类
    /// </summary>
    public abstract class BaseDto : BaseDto<Guid>
    {

    }
}
