﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
    public class UserRoleRepository : RepositoryBase<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
