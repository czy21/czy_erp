﻿using System;
using System.Collections.Generic;
using ERP.Common.Model;
using Microsoft.AspNetCore.Http;

namespace ERP.Web.Core.Pocket
{
    [AttributeUsage(validOn: AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class PocketAttribute : Attribute
    {
        private Type TargetType { get; }
        private string KeyName { get; }

        public PocketAttribute(Type targetType, string keyName)
        {
            TargetType = targetType;
            KeyName = keyName;
        }

        internal Tuple<string, object> ObtainResult(HttpContext ctx)
        {
            var res = ctx.RequestServices.ObtainPocketResult<HttpContext, IList<SimpleItemModel>>(ctx, TargetType);
            return new Tuple<string, object>(KeyName, res);
        }
    }
}
