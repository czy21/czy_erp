﻿using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.RoleApp.Dtos
{
    /// <summary>
    /// 角色映射实体
    /// </summary>
    public class RoleDto : BaseDto
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色说明
        /// </summary>
        public string Comment { get; set; }
    }
}
