﻿using System;
using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.CategoryApp.Dtos
{
    /// <summary>
    /// 商品类别映射实体
    /// </summary>
    public class CategoryDto : BaseDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public short? Level { get; set; }

        /// <summary>
        /// 上级Id
        /// </summary>
        public Guid ParentId { get; set; }
    }
}
