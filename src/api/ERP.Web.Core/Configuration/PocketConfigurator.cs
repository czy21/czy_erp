﻿using ERP.Common.Extensions.Entity;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.Web.Core.Configuration
{
    public static class PocketConfigurator
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            services.AddPocket<User>(ctx => ctx.GetDbContext().Set<User>().ConvertToSimple());
            services.AddPocket<Menu>(ctx => ctx.GetDbContext().Set<Menu>().ConvertToSimple());
            services.AddPocket<Role>(ctx => ctx.GetDbContext().Set<Role>().ConvertToSimple());
            services.AddPocket<Category>(ctx => ctx.GetDbContext().Set<Category>().ConvertToSimple());
            services.AddPocket<Department>(ctx => ctx.GetDbContext().Set<Department>().ConvertToSimple());
            services.AddPocket<Depot>(ctx => ctx.GetDbContext().Set<Depot>().ConvertToSimple());
            return services;
        }
    }
}
