﻿using System;
using System.Threading.Tasks;
using ERP.BLL.DepotApp;
using ERP.BLL.DepotApp.Dtos;
using ERP.Common.Model;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class DepotController : OperationControllerBase
    {
        private readonly IDepotService _depotService;

        public DepotController(IDepotService depotService)
        {
            _depotService = depotService;
        }

        #region 获取分页仓库列表

        [HttpGet("GetPageDepots")]
        public Task<PagedResultModel<DepotDto>> GetPageDepots(int pageIndex, int pageSize)
        {
            return Task.FromResult(_depotService.GetDepotPageList(pageIndex, pageSize));
        }

        #endregion

        #region 添加仓库

        [HttpPost("Create")]
        public Task<string> Create(DepotDto dto)
        {
            string msg;
            var role = _depotService.Insert(dto);
            switch (role)
            {
                case 1:
                    msg = "depotName already exist";
                    break;
                default:
                    msg = "depot add success";
                    break;
            }

            return Task.FromResult(msg);
        }

        #endregion

        #region 更新仓库

        [HttpPut("Edit")]
        public Task<bool> Edit(DepotDto dto)
        {
            return Task.FromResult(_depotService.Update(dto));
        }

        #endregion

        #region 删除仓库

        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _depotService.Delete(id);
            return Task.FromResult(true);
        }

        #endregion
    }
}