﻿using System.Collections.Generic;
using ERP.BLL.BaseApp;
using ERP.BLL.CompanyApp.Dtos;

namespace ERP.BLL.CompanyApp
{
    public interface ICompanyService : IServiceBase
    {
        /// <summary>
        /// 获取公司实体
        /// </summary>
        CompanyDto GetCompany();

        /// <summary>
        /// 获取公司实体集合
        /// </summary>
        /// <returns></returns>
        List<CompanyDto> GetCompanyList();

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        bool Update(CompanyDto dto);
    }
}
