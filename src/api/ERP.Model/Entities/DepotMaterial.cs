﻿using System;

namespace ERP.Model.Entities
{
    public class DepotMaterial:BaseEntity
    {
        public Guid DepotId { get; set; }

        public Depot Depot { get; set; }

        public Guid MaterialId { get; set; }

        public Material Material { get; set; }
    }
}
