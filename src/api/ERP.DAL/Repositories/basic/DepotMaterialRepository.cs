﻿using ERP.IDAL.IRepositories.basic;
using ERP.EFCore;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
     public class DepotMaterialRepository : RepositoryBase<DepotMaterial>,IDepotMaterialRepository
    {   
        public DepotMaterialRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
