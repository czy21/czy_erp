﻿using System;
using System.Collections.Generic;
using ERP.BLL.BaseApp;
using ERP.BLL.Common;
using ERP.BLL.MenuApp.Dtos;
using ERP.BLL.Model;
using ERP.Common.Model;

namespace ERP.BLL.MenuApp
{
    public interface IMenuService : IServiceBase
    {

        /// <summary>
        /// 获取权限列表
        /// </summary>
        /// <returns></returns>
        List<MenuDto> GetActionList();

        /// <summary>
        /// 根据父级Id获取权限分页列表
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <returns></returns>
        PagedResultModel<MenuDto> GetActionPageListByMenu(SearchActionModel search);

        /// <summary>
        /// 根据用户账号获取菜单列表
        /// </summary>
        /// <param name="loginName">用户账号</param>
        /// <returns></returns>
        List<MenuDto> GetMenusByUser(string loginName);

        /// <summary>
        /// 根据用户账号获取权限列表
        /// </summary>
        /// <param name="loginName">用户账号</param>
        /// <returns></returns>
        List<MenuDto> GetActionsByUser(string loginName);

        /// <summary>
        /// 获取所有用户的权限列表
        /// </summary>
        /// <returns></returns>
        List<UserPermission> GetUsersPermissions();

        /// <summary>
        /// 获取所有角色的权限列表
        /// </summary>
        /// <returns></returns>
        List<RolePermission> GetRolesPermissions();

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="dto">菜单实体</param>
        /// <returns>默认可以添加=0,重名=1,重地址=2</returns>
        int Insert(MenuDto dto);

        /// <summary>
        /// 批量添加权限
        /// </summary>
        /// <param name="dtos">实体集合</param>
        /// <returns></returns>
        bool BatchInsertAction(List<ActionDto> dtos);

        /// <summary>
        /// 检查菜单实体属性值是否存在
        /// </summary>
        /// <param name="attr">菜单属性</param>
        /// <param name="attrValue">属性值</param>
        /// <returns>true==存在重名 false==不存在</returns>
        bool CheckIsExist(string attr, string attrValue);

        /// <summary>
        /// 更新菜单或权限
        /// </summary>
        /// <param name="dto">菜单实体</param>
        /// <returns>bool</returns>
        bool Update(MenuDto dto);

        /// <summary>
        /// 根据Id删除菜单或权限
        /// </summary>
        /// <param name="id">菜单或权限id</param>
        void Delete(Guid id);

        /// <summary>
        /// 获取树形权限列表
        /// </summary>
        /// <returns></returns>
        List<SimpleTreeDto> GetActionTree();
    }
}