﻿using System;
using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.DepartmentApp.Dtos
{
    /// <summary>
    /// 部门映射实体
    /// </summary>
    public class DepartmentDto : BaseDto
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        public Guid ParentId { get; set; }
        
        /// <summary>
        /// 部门电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 部门说明
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 所属公司Id
        /// </summary>
        public Guid CompanyId { get; set; }
    }
}
