﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.BLL.CategoryApp;
using ERP.BLL.CategoryApp.Dtos;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class CategoryController : OperationControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        #region 获取分页商品类别列表
        [HttpGet("GetCategories")]
        public Task<List<CategoryDto>> GetCategories()
        {
            return Task.FromResult(_categoryService.GetCategoryList());
        }
        #endregion

        #region 添加商品类别
        [HttpPost("Create")]
        public Task<string> Create(CategoryDto dto)
        {
            string msg;
            var role = _categoryService.Insert(dto);
            switch (role)
            {
                case 1:
                    msg = "categoryName already exist";
                    break;
                default:
                    msg = "category add success";
                    break;
            }
            return Task.FromResult(msg);
        }
        #endregion

        #region 更新商品类别
        [HttpPut("Edit")]
        public Task<bool> Edit(CategoryDto dto)
        {
            return Task.FromResult(_categoryService.Update(dto));
        }
        #endregion

        #region 删除商品类别
        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _categoryService.Delete(id);
            return Task.FromResult(true);
        }
        #endregion
    }
}