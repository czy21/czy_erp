﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.BLL.MenuApp;
using ERP.BLL.MenuApp.Dtos;
using ERP.BLL.Model;
using ERP.Common.Model;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class MenuController : OperationControllerBase
    {
        private readonly IMenuService _service;

        public MenuController(IMenuService service)
        {
            _service = service;
        }

        #region 获取用户菜单列表

        [HttpGet("GetUserMenus")]
        public Task<List<MenuDto>> GetUserMenus(string loginName)
        {
            return Task.FromResult(_service.GetMenusByUser(loginName));
        }

        #endregion



        #region 根据菜单获取分页权限列表

        [HttpPost("GetPageMenuActions")]
        [Pocket(typeof(Menu), "menus")]
        public Task<PagedResultModel<MenuDto>> GetPageMenuActions(SearchActionModel input)
        {
            return Task.FromResult(_service.GetActionPageListByMenu(input));
        }

        #endregion

        #region 添加菜单

        [HttpPost("Create")]
        public Task<string> Create(MenuDto dto)
        {
            string msg;
            var menu = _service.Insert(dto);
            switch (menu)
            {
                case 1:
                    msg = "name already exist";
                    break;
                case 2:
                    msg = "url already exist";
                    break;
                default:
                    msg = "menu add success";
                    break;
            }

            return Task.FromResult(msg);
        }

        #endregion

        #region 更新菜单

        [HttpPut("Edit")]
        public Task<bool> Edit(MenuDto dto)
        {
            return Task.FromResult(_service.Update(dto));
        }

        #endregion

        #region 删除菜单

        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _service.Delete(id);

            return Task.FromResult(true);
        }

        #endregion

        #region 批量添加权限

        [HttpPost("BatchAddAction")]
        public Task<bool> BatchAddAction(List<ActionDto> actions)
        {
            return Task.FromResult(_service.BatchInsertAction(actions));
        }

        #endregion
    }
}