﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ERP.Web.Core.Facade;
using Microsoft.AspNetCore.Authorization;

namespace ERP.Web.Mvc.Basic
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("MyDomain")]
    public class OperationControllerBase : ApiControllerBase
    {

    }
}