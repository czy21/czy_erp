﻿using System;
using ERP.BLL.BaseApp.Dtos;

namespace ERP.BLL.MenuApp.Dtos
{
    /// <summary>
    /// 菜单映射实体
    /// </summary>
    public class MenuDto : BaseDto
    {
        /// <summary>
        /// 上级菜单
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { get; set; }
        
        /// <summary>
        /// 菜单排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 是否菜单
        /// </summary>
        public bool IsMenu { get; set; }

        /// <summary>
        /// 菜单说明
        /// </summary>
        public string Comment { get; set; }
    }
}
