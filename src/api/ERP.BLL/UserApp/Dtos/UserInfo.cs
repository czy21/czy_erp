﻿namespace ERP.BLL.UserApp.Dtos
{
    public class UserInfo
    {
        /// <summary>
        /// 用户账号
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户说明
        /// </summary>
        public string Comment { get; set; }


    }
}
