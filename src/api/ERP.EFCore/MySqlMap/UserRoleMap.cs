﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class UserRoleMap : IBaseEntityMap<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable("erp_user_role");
            builder.HasKey(r => r.Id);
            
            builder.HasOne(r => r.User).WithMany(t => t.UserRoles)
                 .HasForeignKey(fk => fk.UserId);
            builder.HasOne(r => r.Role).WithMany(t => t.UserRoles)
                .HasForeignKey(fk => fk.RoleId);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
