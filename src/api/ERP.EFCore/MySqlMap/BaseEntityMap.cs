﻿using ERP.Model;
using Microsoft.EntityFrameworkCore;

namespace ERP.EFCore.MySqlMap
{
    public interface IBaseEntityMap<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : BaseEntity
    {
        
    }
}
