﻿using ERP.BLL.CompanyApp;
using ERP.BLL.CompanyApp.Dtos;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Web.Mvc.Controllers
{
    public class CompanyController : OperationControllerBase
    {
        private readonly ICompanyService _service;
        public CompanyController(ICompanyService service)
        {
            _service = service;
        }

        #region 获取公司信息
        [HttpGet("GetCompanies")]
        [Pocket(typeof(Department), "departments")]
        public Task<List<CompanyDto>> GetCompanies()
        {
            return Task.FromResult(_service.GetCompanyList());
        }
        #endregion

        public Task<bool> Edit(CompanyDto dto)
        {
            return Task.FromResult(_service.Update(dto));
        }
    }
}