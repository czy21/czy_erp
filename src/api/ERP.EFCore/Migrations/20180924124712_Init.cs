﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.EFCore.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "erp_category",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Level = table.Column<short>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "erp_company",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ContactPerson = table.Column<string>(maxLength: 20, nullable: true),
                    Location = table.Column<string>(maxLength: 100, nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Fax = table.Column<string>(maxLength: 20, nullable: true),
                    Postcode = table.Column<string>(maxLength: 20, nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "erp_depot",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 50, nullable: true),
                    DepotCharge = table.Column<double>(nullable: true),
                    Truckage = table.Column<double>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    Sort = table.Column<int>(nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_depot", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "erp_menu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    ParentId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Icon = table.Column<string>(maxLength: 100, nullable: true),
                    Sort = table.Column<int>(nullable: false, defaultValue: 0),
                    Url = table.Column<string>(maxLength: 50, nullable: true),
                    IsMenu = table.Column<bool>(nullable: false, defaultValue: false),
                    Comment = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "erp_role",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "erp_material",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Manufacturer = table.Column<string>(maxLength: 50, nullable: true),
                    Packing = table.Column<double>(nullable: true),
                    SafetyStock = table.Column<double>(nullable: true),
                    Model = table.Column<string>(maxLength: 20, nullable: true),
                    Standard = table.Column<string>(maxLength: 20, nullable: true),
                    Color = table.Column<string>(maxLength: 20, nullable: true),
                    Unit = table.Column<string>(maxLength: 20, nullable: true),
                    RetailPrice = table.Column<double>(nullable: true),
                    LowPrice = table.Column<double>(nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_material", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_material_erp_category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "erp_category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "erp_department",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ParentId = table.Column<Guid>(nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true),
                    CompanyId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_department", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_department_erp_company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "erp_company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "erp_role_menu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    RoleId = table.Column<Guid>(nullable: false),
                    MenuId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_role_menu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_role_menu_erp_menu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "erp_menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_erp_role_menu_erp_role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "erp_role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "erp_depot_material",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    DepotId = table.Column<Guid>(nullable: false),
                    MaterialId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_depot_material", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_depot_material_erp_depot_DepotId",
                        column: x => x.DepotId,
                        principalTable: "erp_depot",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_erp_depot_material_erp_material_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "erp_material",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "erp_user",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    LoginName = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 100, nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    IsHeader = table.Column<bool>(nullable: false, defaultValue: false),
                    DepartmentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_user", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_user_erp_department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "erp_department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "erp_user_role",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Enabled = table.Column<bool>(nullable: false, defaultValue: true),
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_erp_user_role", x => x.Id);
                    table.ForeignKey(
                        name: "FK_erp_user_role_erp_role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "erp_role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_erp_user_role_erp_user_UserId",
                        column: x => x.UserId,
                        principalTable: "erp_user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_erp_department_CompanyId",
                table: "erp_department",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_depot_material_DepotId",
                table: "erp_depot_material",
                column: "DepotId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_depot_material_MaterialId",
                table: "erp_depot_material",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_material_CategoryId",
                table: "erp_material",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_role_menu_MenuId",
                table: "erp_role_menu",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_role_menu_RoleId",
                table: "erp_role_menu",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_user_DepartmentId",
                table: "erp_user",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_user_role_RoleId",
                table: "erp_user_role",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_erp_user_role_UserId",
                table: "erp_user_role",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "erp_depot_material");

            migrationBuilder.DropTable(
                name: "erp_role_menu");

            migrationBuilder.DropTable(
                name: "erp_user_role");

            migrationBuilder.DropTable(
                name: "erp_depot");

            migrationBuilder.DropTable(
                name: "erp_material");

            migrationBuilder.DropTable(
                name: "erp_menu");

            migrationBuilder.DropTable(
                name: "erp_role");

            migrationBuilder.DropTable(
                name: "erp_user");

            migrationBuilder.DropTable(
                name: "erp_category");

            migrationBuilder.DropTable(
                name: "erp_department");

            migrationBuilder.DropTable(
                name: "erp_company");
        }
    }
}
