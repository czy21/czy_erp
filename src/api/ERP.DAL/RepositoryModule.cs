﻿using Autofac;
using ERP.IDAL;

namespace ERP.DAL
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EfUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => t.IsAssignableTo<IRepositoryBase>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
