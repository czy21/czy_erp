﻿using System;
using System.Collections.Generic;
using ERP.BLL.BaseApp;
using ERP.BLL.RoleApp.Dtos;
using ERP.Common.Model;

namespace ERP.BLL.RoleApp
{
    public interface IRoleService : IServiceBase
    {
        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        List<RoleDto> GetRoleList();

        /// <summary>
        /// 获取分页角色列表
        /// </summary>
        /// <param name="startPage">起始页</param>
        /// <param name="pageSize">单页大小</param>
        /// <param name="rowCount">总行数</param>
        /// <returns></returns>
        PagedResultModel<RoleDto> GetRolePageList(int startPage, int pageSize);

        /// <summary>
        /// 根据id获取角色
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        RoleDto GetRole(Guid id);

        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="dto">角色实体</param>
        /// <returns>默认可以添加=0,重名=1</returns>
        int Insert(RoleDto dto);

        /// <summary>
        /// 更新角色
        /// </summary>
        /// <param name="dto">角色实体</param>
        /// <returns></returns>
        bool Update(RoleDto dto);

        /// <summary>
        /// 根据Id删除角色
        /// </summary>
        /// <param name="id">主键</param>
        void Delete(Guid id);

        /// <summary>
        /// 获取角色菜单集合
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        List<Guid> GetMenusByRole(Guid roleId);

        /// <summary>
        /// 获取角色权限集合
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        List<Guid> GetActionsByRole(Guid roleId);


        /// <summary>
        /// 更新角色菜单关联关系
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <param name="roleMenus">角色菜单集合</param>
        /// <returns></returns>
        bool UpdateRoleMenu(Guid roleId, List<RoleMenuDto> roleMenus);

        /// <summary>
        /// 更新角色权限关联关系
        /// </summary>
        /// <param name="roleId">角色id</param>
        /// <param name="roleActions">角色权限集合</param>
        /// <returns></returns>
        bool UpdateRoleAction(Guid roleId, List<RoleMenuDto> roleActions);
    }
}