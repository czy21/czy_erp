﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Core.Model
{
    public class ObjectResultModel : ObjectResult
    {
        public ObjectResultModel(Dictionary<string, object> value)
            : base(value)
        {
        }
    }
}
