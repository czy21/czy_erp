﻿using System.Collections.Generic;
using System.Linq;
using ERP.Common.Model;
using ERP.Model.Entities;

namespace ERP.Common.Extensions.Entity
{
    public static class MenuExtension
    {
        public static IList<SimpleItemModel> ConvertToSimple(this IEnumerable<Menu> menus)
        {
            return menus?.Where(t => t.IsMenu).Select(s => new SimpleItemModel
            {
                Label = s.Name,
                Value = s.Id.ToString(),
                ParentId = s.ParentId.ToString()
            }).ToList();
        }
    }
}
