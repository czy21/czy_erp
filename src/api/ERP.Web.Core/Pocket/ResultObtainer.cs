﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ERP.Web.Core.Pocket
{
    public class ResultObtainer<TParameter, TResult> where TResult : class
    {
        private readonly IDictionary<object, Func<TParameter, TResult>> _resultBuilders = new ConcurrentDictionary<object, Func<TParameter, TResult>>();

        public ResultObtainer(object lane, Func<TParameter, TResult> resultBuilder)
        {
            _resultBuilders[lane] = resultBuilder;
        }
        public void RegisterLane(object lane, Func<TParameter, TResult> resultBuilder)
        {
            _resultBuilders[lane] = resultBuilder;
        }
        private Func<TParameter, TResult> RetrieveResultBuilder(object lane)
        {
            return _resultBuilders.TryGetValue(lane, out var res) ? res : null;
        }

        public TResult ObtainResult(TParameter parameter, object lane)
        {
            return RetrieveResultBuilder(lane).Invoke(parameter);
        }
    }
}
