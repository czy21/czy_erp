﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.BLL.CategoryApp;
using ERP.BLL.MaterialApp;
using ERP.BLL.MaterialApp.Dtos;
using ERP.BLL.Model;
using ERP.Common.Model;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class MaterialController : OperationControllerBase
    {
        private readonly IMaterialService _materialService;
        private readonly ICategoryService _categoryService;
        public MaterialController(IMaterialService materialService, ICategoryService categoryService)
        {
            _materialService = materialService;
            _categoryService = categoryService;
        }

        #region 获取商品列表
        [HttpPost("GetPageMaterials")]
        [Pocket(typeof(Category), "categories")]
        [Pocket(typeof(Depot), "depots")]
        public Task<PagedResultModel<MaterialDto>> GetPageMaterials(SearchMaterialModel input)
        {
            return Task.FromResult(_materialService.GetMaterialPageList(input));
        }
        #endregion

        #region 根据商品类别获取商品列表
        [HttpGet("GetCategoryMaterials")]
        public Task<List<MaterialDto>> GetCategoryMaterials(Guid categoryId)
        {
            return Task.FromResult(_materialService.GetMaterialsByCategory(categoryId));
        }
        #endregion

        #region 获取商品仓库列表
        [HttpGet("GetMaterialDepots")]
        public Task<List<Guid>> GetMaterialDepots(Guid materialId)
        {
            return Task.FromResult(_materialService.GetDepotsByMaterial(materialId));
        }
        #endregion

        #region 获取仓库商品列表
        [HttpGet("GetDepotMaterials")]
        public Task<List<MaterialDto>> GetDepotMaterials(Guid depotId)
        {
            return Task.FromResult(_materialService.GetMaterialsByDepot(depotId));
        }
        #endregion

        #region 保存商品仓库
        [HttpPost("SaveMaterialDepots")]
        public Task<bool> SaveMaterialDepots(Guid materialId, List<DepotMaterialDto> materialDepots)
        {
            return Task.FromResult(_materialService.UpdateMaterialDepot(materialId, materialDepots));
        }

        #endregion

        #region 添加商品
        [HttpPost("Create")]
        public Task<string> Create(MaterialDto dto)
        {
            string msg;
            var role = _materialService.Insert(dto);
            switch (role)
            {
                case 1:
                    msg = "materialName already exist";
                    break;
                default:
                    msg = "material add success";
                    break;
            }
            return Task.FromResult(msg);
        }
        #endregion
        #region 更新商品

        [HttpPut("Edit")]
        public Task<bool> Edit(MaterialDto dto)
        {
            return Task.FromResult(_materialService.Update(dto));
        }

        #endregion
        #region 删除商品类别
        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _materialService.Delete(id);
            return Task.FromResult(true);
        }
        #endregion
    }
}