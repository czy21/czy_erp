﻿using System;
using ERP.BLL.BaseApp;
using ERP.BLL.CategoryApp.Dtos;
using System.Collections.Generic;
using ERP.Common.Model;

namespace ERP.BLL.CategoryApp
{
    public interface ICategoryService : IServiceBase
    {

        /// <summary>
        /// 获取分页商品类别列表
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页数</param>
        /// <returns></returns>
        PagedResultModel<CategoryDto> GetCategoryPageList(int pageIndex, int pageSize);

        /// <summary>
        /// 获取商品类别列表
        /// </summary>
        /// <returns></returns>
        List<CategoryDto> GetCategoryList();


        /// <summary>
        /// 添加商品类别
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        int Insert(CategoryDto dto);

        /// <summary>
        /// 更新商品类别
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        bool Update(CategoryDto dto);

        /// <summary>
        /// 根据Id删除商品类别
        /// </summary>
        /// <param name="id"></param>
        void Delete(Guid id);

    }
}
