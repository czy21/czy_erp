﻿using ERP.Model.Entities;

namespace ERP.IDAL.IRepositories.basic
{
    public interface IRoleRepository : IRepositoryBase<Role>
    {
       
    }

}
