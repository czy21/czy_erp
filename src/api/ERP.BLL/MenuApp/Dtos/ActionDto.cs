﻿namespace ERP.BLL.MenuApp.Dtos
{
    public class ActionDto
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string FunctionName { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControlName { get; set; }

        /// <summary>
        /// 方法名称
        /// </summary>
        public string MethodName { get; set; }

    }
}
