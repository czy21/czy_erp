﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.BLL.MenuApp;
using ERP.BLL.RoleApp;
using ERP.BLL.RoleApp.Dtos;
using ERP.Common.Model;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class RoleController : OperationControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMenuService _menuService;

        public RoleController(IRoleService roleService, IMenuService menuService)
        {
            _roleService = roleService;
            _menuService = menuService;
        }


        #region 获取角色列表

        [HttpGet("GetRoles")]
        public Task<List<RoleDto>> GetRoles()
        {
            return Task.FromResult(_roleService.GetRoleList());
        }

        #endregion

        #region 获取分页角色列表

        [HttpGet("GetPageRoles")]
        [Pocket(typeof(Menu), "menus")]
        public Task<PagedResultModel<RoleDto>> GetPageRoles(int pageIndex, int pageSize)
        {
            return Task.FromResult(_roleService.GetRolePageList(pageIndex, pageSize));
        }

        #endregion

        #region 添加角色

        [HttpPost("Create")]
        public Task<string> Create(RoleDto dto)
        {
            string msg;
            var role = _roleService.Insert(dto);
            switch (role)
            {
                case 1:
                    msg = "roleName already exist";
                    break;
                default:
                    msg = "role add success";
                    break;
            }

            return Task.FromResult(msg);
        }

        #endregion

        #region 更新角色

        [HttpPut("Edit")]
        public Task<bool> Edit(RoleDto dto)
        {
            return Task.FromResult(_roleService.Update(dto));
        }

        #endregion

        #region 删除角色

        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _roleService.Delete(id);
            return Task.FromResult(true);
        }

        #endregion

        #region 保存角色菜单

        [HttpPost("SaveRoleMenus")]
        public Task<bool> SaveRoleMenu(Guid roleId, List<RoleMenuDto> roleMenus)
        {
            return Task.FromResult(_roleService.UpdateRoleMenu(roleId, roleMenus));
        }

        #endregion

        #region 保存角色权限

        [HttpPost("SaveRoleActions")]
        public Task<bool> SaveRoleAction(Guid roleId, List<RoleMenuDto> roleActions)
        {
            return Task.FromResult(_roleService.UpdateRoleAction(roleId, roleActions));
        }

        #endregion

        #region 获取角色菜单

        [HttpGet("GetRoleMenus")]
        public Task<List<Guid>> GetRoleMenus(Guid roleId)
        {
            return Task.FromResult(_roleService.GetMenusByRole(roleId));
        }

        #endregion

        #region 获取角色权限

        [HttpGet("GetRoleActions")]
        public Task<object> GetRoleActions(Guid roleId)
        {
            var actionTree = _menuService.GetActionTree();
            var roleActions = _roleService.GetActionsByRole(roleId);
            object obj = new {actionTree, roleActions};
            return Task.FromResult(obj);
        }

        #endregion
    }
}