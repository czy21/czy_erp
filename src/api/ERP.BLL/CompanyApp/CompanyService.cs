﻿using System.Collections.Generic;
using AutoMapper;
using ERP.BLL.CompanyApp.Dtos;
using ERP.IDAL.IRepositories.basic;

namespace ERP.BLL.CompanyApp
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _repository;

        public CompanyService(ICompanyRepository repository)
        {
            _repository = repository;
        }

        public CompanyDto GetCompany()
        {
            return Mapper.Map<CompanyDto>(_repository.FirstOrDefaultAsync(r => true));
        }

        public List<CompanyDto> GetCompanyList()
        {
            return Mapper.Map<List<CompanyDto>>(_repository.GetAllListAsync().Result);
        }

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public bool Update(CompanyDto dto)
        {
            //var Company = _repository.Update(Mapper.Map<Company>(dto));
            //return Company == null ? false : true;
            return true;
        }
    }
}