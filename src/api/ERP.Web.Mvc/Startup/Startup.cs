﻿using ERP.BLL;
using ERP.Core.Extensions;
using ERP.EFCore;
using ERP.Web.Core.Authentication.JwtBearer;
using ERP.Web.Core.Configuration;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace ERP.Web.Mvc.Startup
{
    public class Startup
    {
        private IConfigurationRoot AppConfiguration { get; }
        public Startup(IHostingEnvironment env)
        {
            AppConfiguration = env.GetAppConfiguration();
            XmlConfigurator.Configure(LogManager.CreateRepository("NETCoreRepository"), new FileInfo("log4net.config"));
            EntitiesMapper.Initialize();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            DbConfigurator.Configure(services, AppConfiguration);
            CorsConfigurator.Configure(services);
            AuthConfigurator.Configure(services, AppConfiguration);
            PocketConfigurator.Configure(services);
            MvcConfigurator.Configure(services);
            return AutofacConfigurator.Configure(services, AppConfiguration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseCors("AllowSameDomain");

            app.UseJwtAuth();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            SeedData.Initialize(app.ApplicationServices); //初始化数据
        }
    }
}