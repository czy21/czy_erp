﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    [Authorize]
    [EnableCors("MyDomain")]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public IActionResult Index()
        {
            return Redirect("/dist/index.html");
        }
    }
}