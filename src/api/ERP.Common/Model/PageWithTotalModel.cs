﻿namespace ERP.Common.Model
{
    public class PageWithTotalModel
    {
        public int TotalCount
        {
            get;
            set;
        }
        public int PageSize
        {
            get;
            set;
        }
        public int PageIndex
        {
            get;
            set;
        }
    }
}
