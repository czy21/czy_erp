﻿using ERP.EFCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.Web.Core.Configuration
{
    public static class DbConfigurator
    {
        public static IServiceCollection Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseMySql(configuration.GetConnectionString("MySqlConnection")));
            services.AddScoped<DbContext>(provider => provider.GetService<DataContext>());
            return services;
        }
    }
}
