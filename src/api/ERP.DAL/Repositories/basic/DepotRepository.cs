﻿using ERP.IDAL.IRepositories.basic;
using ERP.EFCore;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
     public class DepotRepository : RepositoryBase<Depot>,IDepotRepository
    {   
        public DepotRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
