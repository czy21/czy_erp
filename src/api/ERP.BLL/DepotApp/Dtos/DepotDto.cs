﻿using ERP.BLL.BaseApp.Dtos;
namespace ERP.BLL.DepotApp.Dtos
{
    /// <summary>
    /// 仓库映射实体
    /// </summary>
    public class DepotDto : BaseDto
    {
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 仓库地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 仓储费
        /// </summary>
        public double? DepotCharge { get; set; }

        /// <summary>
        /// 搬运费
        /// </summary>
        public double? Truckage { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int? Sort { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string Comment { get; set; }
    }
}
