﻿using System;

namespace ERP.BLL.Model
{
    public class SearchActionModel : PagableCriteria
    {
        public Guid ParentId { get; set; }
    }
}
