﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class MaterialMap : IBaseEntityMap<Material>
    {
        public void Configure(EntityTypeBuilder<Material> builder)
        {
            builder.ToTable("erp_material");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Name).HasMaxLength(50);
            builder.Property(t => t.Manufacturer).HasMaxLength(50);
            builder.Property(t => t.Model).HasMaxLength(20);
            builder.Property(t => t.Standard).HasMaxLength(20);
            builder.Property(t => t.Color).HasMaxLength(20);
            builder.Property(t => t.Unit).HasMaxLength(20);
            builder.Property(r => r.Comment).HasMaxLength(100);

            builder.HasOne(t => t.Category).WithMany(t => t.Materials)
                .HasForeignKey(fk => fk.CategoryId);

            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");

        }
    }
}
