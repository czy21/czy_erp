﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
     public class MenuRepository : RepositoryBase<Menu>,IMenuRepository
    {   
        public MenuRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
