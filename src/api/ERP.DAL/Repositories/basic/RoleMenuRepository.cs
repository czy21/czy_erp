﻿using ERP.EFCore;
using ERP.IDAL.IRepositories.basic;
using ERP.Model.Entities;

namespace ERP.DAL.Repositories.basic
{
    public class RoleMenuRepository : RepositoryBase<RoleMenu>, IRoleMenuRepository
    {
        public RoleMenuRepository(DataContext dbContext) : base(dbContext)
        {

        }
    }
}
