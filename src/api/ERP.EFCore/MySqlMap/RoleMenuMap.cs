﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class RoleMenuMap : IBaseEntityMap<RoleMenu>
    {
        public void Configure(EntityTypeBuilder<RoleMenu> builder)
        {
            builder.ToTable("erp_role_menu");
            builder.HasKey(r => r.Id);

            builder.HasOne(r => r.Menu).WithMany(t => t.RoleMenus)
                 .HasForeignKey(fk => fk.MenuId);
            builder.HasOne(r => r.Role).WithMany(t => t.RoleMenus)
                .HasForeignKey(fk => fk.RoleId);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
