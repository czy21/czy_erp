﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class DepotMap : IBaseEntityMap<Depot>
    {
        public void Configure(EntityTypeBuilder<Depot> builder)
        {
            builder.ToTable("erp_depot");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Name).HasMaxLength(50);
            builder.Property(t => t.Address).HasMaxLength(50);
            builder.Property(t => t.Comment).HasMaxLength(100);

            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
