﻿using ERP.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.EFCore.MySqlMap
{
    public class MenuMap : IBaseEntityMap<Menu>
    {
        public void Configure(EntityTypeBuilder<Menu> builder)
        {
            builder.ToTable("erp_menu");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.Name).HasMaxLength(50);
            builder.Property(r => r.Icon).HasMaxLength(100);
            builder.Property(r => r.Url).HasMaxLength(50);
            builder.Property(r => r.Sort).HasDefaultValue(0);
            builder.Property(r => r.IsMenu).HasDefaultValue(0);
            builder.Property(r => r.Comment).HasMaxLength(100);
            builder.Property(r => r.Enabled).HasDefaultValue(1);
            builder.Property(t => t.AddedTime).HasColumnType("datetime");
            builder.Property(t => t.ModifiedTime).HasColumnType("datetime");
        }
    }
}
