﻿using System;

namespace ERP.Common.Exceptions
{
    public class EntityNotFoundExc : Exception
    {
        public Type EntityType { get; set; }


        public object Id { get; set; }

        public EntityNotFoundExc()
        {

        }
        public EntityNotFoundExc(Type entityType, object id)
            : base($"There is no such an entity. Entity type: {entityType.FullName}, id: {id}")
        {
            EntityType = entityType;
            Id = id;
        }

    }
}
