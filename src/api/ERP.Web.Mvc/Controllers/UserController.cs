﻿using ERP.BLL.UserApp;
using ERP.BLL.UserApp.Dtos;
using ERP.Common.Model;
using ERP.Model.Entities;
using ERP.Web.Core.Pocket;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Web.Mvc.Controllers
{
    public class UserController : OperationControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        #region 获取分页用户列表

        [HttpGet("GetPageUsers")]
        [Pocket(typeof(Role), "roles")]
        [Pocket(typeof(Department), "departments")]
        public Task<PagedResultModel<UserDto>> GetPageUsers(int pageIndex, int pageSize)
        {
            return Task.FromResult(_service.GetUserPageList(pageIndex, pageSize));
        }

        #endregion

        #region 添加用户

        [HttpPost("Create")]
        public Task<string> Create(UserDto dto)
        {
            string msg;
            var user = _service.Insert(dto);
            switch (user)
            {
                case 1:
                    msg = "loginName already exist";
                    break;
                case 2:
                    msg = "userName already exist";
                    break;
                default:
                    msg = "user add success";
                    break;
            }

            return Task.FromResult(msg);
        }

        #endregion

        #region 更新用户

        [HttpPut("Edit")]
        public Task<bool> Edit(UserDto dto)
        {
            return Task.FromResult(_service.Update(dto));
        }

        #endregion

        #region 删除用户

        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _service.Delete(id);
            return Task.FromResult(true);
        }

        #endregion

        #region 保存用户角色

        [HttpPost("SaveUserRoles")]
        public Task<bool> SaveUserRoles(Guid userId, List<UserRoleDto> userRoles)
        {
            return Task.FromResult(_service.UpdateUserRole(userId, userRoles));
        }

        #endregion

        #region 获取用户角色列表

        [HttpGet("GetUserRoles")]
        public Task<List<Guid>> GetUserRoles(Guid userId)
        {
            return Task.FromResult(_service.GetRolesByUser(userId));
        }

        #endregion
    }
}