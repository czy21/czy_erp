﻿using System;
using System.Collections.Generic;

namespace ERP.Model.Entities
{
    /// <summary>
    /// 用户实体
    /// </summary>
    public class User : BaseEntity
    {

        /// <summary>
        /// 用户账号
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 是否部长
        /// </summary>
        public bool IsHeader { get; set; }

        /// <summary>
        /// 所属部门Id
        /// </summary>
        public Guid DepartmentId { get; set; }

        /// <summary>
        /// 所属部门实体
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// 用户角色实体集合
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }

    }

}
