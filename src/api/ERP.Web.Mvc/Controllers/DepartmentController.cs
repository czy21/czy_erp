using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.BLL.DepartmentApp;
using ERP.BLL.DepartmentApp.Dtos;
using ERP.Web.Mvc.Basic;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Mvc.Controllers
{
    public class DepartmentController : OperationControllerBase
    {
        private readonly IDepartmentService _service;

        public DepartmentController(IDepartmentService service)
        {
            _service = service;
        }

        [HttpGet("GetDepartments")]
        public Task<List<DepartmentDto>> GetDepartments()
        {
            return Task.FromResult(_service.GetDepartmentList());
        }

        #region 根据公司获取部门列表
        [HttpGet("GetCompanyDeps")]
        public Task<List<DepartmentDto>> GetCompanyDeps(Guid companyId)
        {
            return Task.FromResult(_service.GetDepsByComId(companyId));
        }

        #endregion

        #region 添加部门
        [HttpPost("Create")]
        public Task<string> Create(DepartmentDto dto)
        {
            string msg;
            var dep = _service.Insert(dto);
            switch (dep)
            {
                case 1:
                    msg = "name already exist";
                    break;
                default:
                    msg = "dep add success";
                    break;
            }
            return Task.FromResult(msg);
        }

        #endregion

        #region 修改部门
        [HttpPut("Edit")]
        public Task<bool> Edit(DepartmentDto dto)
        {
            return Task.FromResult(_service.Update(dto));
        }

        #endregion

        #region 删除部门
        [HttpDelete("Delete")]
        public Task<bool> Delete(Guid id)
        {
            _service.Delete(id);
            return Task.FromResult(true);
        }

        #endregion
    }
}