﻿using System;
using ERP.BLL.BaseApp;
using ERP.BLL.MaterialApp.Dtos;
using System.Collections.Generic;
using ERP.BLL.Model;
using ERP.Common.Model;

namespace ERP.BLL.MaterialApp
{
    public interface IMaterialService : IServiceBase
    {
        /// <summary>
        /// 获取商品列表
        /// </summary>
        /// <returns></returns>
        List<MaterialDto> GetMaterialList();

        /// <summary>
        /// 获取分页商品列表
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <returns></returns>
        PagedResultModel<MaterialDto> GetMaterialPageList(SearchMaterialModel search);

        /// <summary>
        /// 根据仓库Id获取商品列表
        /// </summary>
        /// <param name="depotId"></param>
        /// <returns></returns>
        List<MaterialDto> GetMaterialsByDepot(Guid depotId);


        /// <summary>
        /// 根据商品类别获取商品列表
        /// </summary>
        /// <param name="categoryId">商品类别Id</param>
        /// <returns></returns>
        List<MaterialDto> GetMaterialsByCategory(Guid categoryId);

        /// <summary>
        /// 根据商品Id获取仓库集合
        /// </summary>
        /// <param name="materialId">商品Id</param>
        /// <returns></returns>
        List<Guid> GetDepotsByMaterial(Guid materialId);

        /// <summary>
        /// 更新商品仓库关联关系
        /// </summary>
        /// <param name="materialId">商品id</param>
        /// <param name="materialDepots">商品仓库集合</param>
        /// <returns></returns>
        bool UpdateMaterialDepot(Guid materialId, List<DepotMaterialDto> materialDepots);

        /// <summary>
        /// 添加商品
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        int Insert(MaterialDto dto);

        /// <summary>
        /// 更新商品
        /// </summary>
        /// <param name="dto">商品实体</param>
        /// <returns></returns>
        bool Update(MaterialDto dto);

        /// <summary>
        /// 根据Id删除商品
        /// </summary>
        /// <param name="id"></param>
        void Delete(Guid id);
    }
}
