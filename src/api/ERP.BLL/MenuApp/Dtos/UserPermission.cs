﻿namespace ERP.BLL.MenuApp.Dtos
{
    /// <summary>
    /// 用户权限
    /// </summary>
    public class UserPermission
    {
        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public virtual string Url { get; set; }


    }
}
