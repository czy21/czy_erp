﻿using ERP.Web.Core.Facade;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace ERP.Web.Core.Configuration
{
    public static class MvcConfigurator
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add(typeof(CustomResultFilter)))
                .AddJsonOptions(options => { options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore; });
            return services;
        }
    }
}
